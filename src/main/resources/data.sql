DROP TABLE IF EXISTS SCALE;
DROP TABLE IF EXISTS CAR;
DROP TABLE IF EXISTS TRACK;


CREATE TABLE SCALE
(
    id    INT PRIMARY KEY AUTO_INCREMENT,
    unit  VARCHAR(250) NOT NULL,
    value DOUBLE       NOT NULL
);

CREATE TABLE TRACK
(
    id          INT PRIMARY KEY AUTO_INCREMENT ,
    name        VARCHAR(250) NOT NULL,
    description VARCHAR(250) NOT NULL,
    scale_id    INT,
    foreign key (scale_id) references SCALE (id)

);

CREATE TABLE CAR
(
    id           INT PRIMARY KEY AUTO_INCREMENT,
    code         VARCHAR(250) NOT NULL,
    transmission VARCHAR(250) NOT NULL,
    ai           BOOLEAN NOT NULL,
    scale_id     INT,
    track_id     INT,
    foreign key (track_id) references TRACK (id),
    foreign key (scale_id) references SCALE (id)
);

INSERT INTO SCALE (id, unit, value)
VALUES (1, 'mps', 110.12121212);
INSERT INTO SCALE (id, unit, value)
VALUES (2, 'mps', 120.967);
INSERT INTO SCALE (id, unit, value)
VALUES (3, 'km', 7.4);
INSERT INTO TRACK(id ,name, description, scale_id)
values (1, 'Millbrook','Millbrook city course race track',3);
INSERT INTO CAR (id, code, transmission, ai, scale_id, track_id)
VALUES (1, 'rdb1', 'automatic', true, 1, 1);
INSERT INTO CAR (id, code, transmission, ai, scale_id, track_id)
VALUES (2, 'rdb3', 'automatic', false, 2, 1);
