package com.mts.service;

import com.mts.model.Track;

import java.util.List;

public interface TrackService {

    void saveOrUpdate(Track track);

    Track findById(long id);

    List<Track> findAll();

    boolean existById(long id);
}
