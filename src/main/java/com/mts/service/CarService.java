package com.mts.service;

import com.mts.model.Car;

import java.util.List;

public interface CarService {

    Car findById(long id);

    void saveOrUpdate(Car car);

    List<Car> findAll();

    boolean existById(long id);
}
