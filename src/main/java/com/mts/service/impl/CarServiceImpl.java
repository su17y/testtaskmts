package com.mts.service.impl;

import com.mts.dao.CarRepository;
import com.mts.model.Car;
import com.mts.service.CarService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    private final CarRepository carRepository;

    public CarServiceImpl(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Override
    public Car findById(long id) {
        return carRepository.findById(id).orElse(new Car());
    }

    @Override
    public void saveOrUpdate(Car car) {
        carRepository.save(car);
    }

    @Override
    public List<Car> findAll() {
        return carRepository.findAll();
    }

    @Override
    public boolean existById(long id) {
        return carRepository.existsById(id);
    }
}
