package com.mts.service.impl;

import com.mts.dao.TrackRepository;
import com.mts.model.Track;
import com.mts.service.TrackService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrackServiceImpl implements TrackService {

    private final TrackRepository trackRepository;

    public TrackServiceImpl(TrackRepository trackRepository) {
        this.trackRepository = trackRepository;
    }

    @Override
    public void saveOrUpdate(Track track) {
        trackRepository.save(track);
    }

    @Override
    public Track findById(long id) {
        return trackRepository.findById(id).orElse(new Track());
    }

    @Override
    public List<Track> findAll() {
        return trackRepository.findAll();
    }

    @Override
    public boolean existById(long id) {
        return trackRepository.existsById(id);
    }
}
