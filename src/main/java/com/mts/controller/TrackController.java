package com.mts.controller;

import com.mts.model.Car;
import com.mts.model.Track;
import com.mts.service.CarService;
import com.mts.service.TrackService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/tracks")
public class TrackController {

    private final TrackService trackService;

    private final CarService carService;

    public TrackController(TrackService trackService, CarService carService) {

        this.trackService = trackService;
        this.carService = carService;
    }

    @PostMapping
    public void save(@RequestBody Track track){
        trackService.saveOrUpdate(track);
    }

    @PutMapping(value = "/{id}")
    public void update( @PathVariable long id, @RequestBody Track track){
        if(trackService.existById(id)){
            track.setId(id);
            trackService.saveOrUpdate(track);
        }

    }

    @GetMapping(value = "/{id}")
    public Track findById(@PathVariable long id){
        return trackService.findById(id);
    }

    @GetMapping
    public List<Track> findAll(){
        return trackService.findAll();
    }

    @PutMapping(value = "/{id}/cars/{carId}")
    public void addNewCar(@PathVariable long id, @PathVariable long carId){
        if(trackService.existById(id)){
            Track track = trackService.findById(id);
            if(carService.existById(carId)){
                Car car = carService.findById(carId);
                List<Car> carList = track.getCars();
                if(carList.isEmpty()){
                    carList = new ArrayList<>();
                    carList.add(carService.findById(carId));
                }else carList.add(carService.findById(carId));
                track.setCars(carList);
                car.setTrack(track);
                trackService.saveOrUpdate(track);
            }else throw new ResponseStatusException(
                    HttpStatus.NOT_FOUND
            );
        }else throw new ResponseStatusException(
                HttpStatus.NOT_FOUND
        );
    }


}
