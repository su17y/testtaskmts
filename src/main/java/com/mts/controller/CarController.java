package com.mts.controller;

import com.mts.model.Car;
import com.mts.service.CarService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/cars")
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping(value = "/{id}")
    public Car findById(@PathVariable long id) {
        return carService.findById(id);
    }

    @GetMapping
    public List<Car> findAll(){
        return carService.findAll();
    }

    @PutMapping(value = "/{id}")
    public void update(@PathVariable long id, @RequestBody Car car){
        if(carService.existById(id)){
            car.setId(id);
            carService.saveOrUpdate(car);
        }
    }

    @PostMapping
    public void save(@RequestBody Car car){
        carService.saveOrUpdate(car);
    }

}
