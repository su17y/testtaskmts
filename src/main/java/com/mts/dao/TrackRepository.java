package com.mts.dao;

import com.mts.model.Track;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TrackRepository extends CrudRepository<Track,Long> {

    @Override
    List<Track> findAll();
}
