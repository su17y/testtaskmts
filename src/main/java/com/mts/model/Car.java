package com.mts.model;

import com.fasterxml.jackson.annotation.*;

import javax.persistence.*;

@Entity
@Table(name = "CAR")
@JsonIgnoreProperties(value = {"id","track"}, allowSetters = true)
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "code")
    private String code;

    @Column(name = "transmission")
    private String transmission;

    @Column(name = "ai")
    private boolean ai;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "scale_id", referencedColumnName = "id")
    @JsonProperty("max-speed")
    private Scale maxSpeed;


    @ManyToOne
    @JoinColumn(name="track_id")
    private Track track;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }

    public boolean getAi() {
        return ai;
    }

    public void setAi(boolean ai) {
        this.ai = ai;
    }

    public Scale getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(Scale maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public Track getTrack() {
        return track;
    }

    public void setTrack(Track track) {
        this.track = track;
    }
}
