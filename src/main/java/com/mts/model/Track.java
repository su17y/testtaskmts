package com.mts.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "TRACK")
@JsonIgnoreProperties(value = {"id"}, allowSetters = true)
public class Track {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;


    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "scale_id", referencedColumnName = "id")
    private Scale length;


    @OneToMany(mappedBy="track", cascade=CascadeType.ALL)
    private List<Car> cars;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Scale getLength() {
        return length;
    }

    public void setLength(Scale length) {
        this.length = length;
    }

    public List<Car> getCars() {
        return cars;
    }

    public void setCars(List<Car> cars) {
        this.cars = cars;
    }

}
